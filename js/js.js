// Toggle Mobile Menu
	//------------------------------------------------------------------------------
	var menuToggle = $('.mobile-menu-toggle'),
			mobileMenu = $('.main-navigation');

	menuToggle.on('click', function() {
		$(this).toggleClass('active');
		mobileMenu.toggleClass('open');
	});

	$('.menu a.anchorlink').click(function(){
		mobileMenuOn = $('.mobile-menu-toggle').hasClass('active');
		if(mobileMenuOn){
			menuToggle.toggleClass('active');
			mobileMenu.toggleClass('open');
		}
		// console.log(mobileMenu);
	});